datalife_party_card_report
==========================

The party_card_report module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-party_card_report/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-party_card_report)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
